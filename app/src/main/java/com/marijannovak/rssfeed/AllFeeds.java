package com.marijannovak.rssfeed;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by marij on 10.4.2017..
 */

@Root(strict = false, name = "rss")
public class AllFeeds {

    @Element(name = "channel")
    private Channel channel;

    static class Channel {

        @Element (name = "copyright") private String copyright;
        @Element (name = "ttl") private String ttl;
        @Element (name = "lastBuildDate") private String lbd;
        @Element (name = "pubDate") private String pdt;
        @Element (name = "managingEditor") private String me;
        @Element (name = "language") private String lng;
        @Element (name = "description") private String dsc;
        @Element (name = "title") private String title;
        @Element (name = "link") private String link;
        @Element (name = "image", required = false) private String img;
        @Element (name = "url") private String url;

        @ElementList(name = "item", inline = true) private ArrayList<FeedItem> feedItems;

    }
    public ArrayList<FeedItem> getFeedItems(){return channel.feedItems;}
}
