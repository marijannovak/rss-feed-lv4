package com.marijannovak.rssfeed;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by marij on 11.4.2017..
 */

public class DatabaseHelper extends SQLiteOpenHelper {


        private static DatabaseHelper Instance = null;

        private DatabaseHelper(Context context)
        {
            super(context.getApplicationContext(),Blueprint.DATABASE_NAME, null, Blueprint.VERSION);

        }

        public static synchronized DatabaseHelper getInstance(Context context)
        {
            if(Instance == null)
            {
                Instance = new DatabaseHelper(context);
            }

            return Instance;
        }

        private static final String SELECT_ALL_FEEDS = "SELECT " + Blueprint.TITLE + ","+ Blueprint.DESCRIPTION + ","+
                Blueprint.TIME + "," + Blueprint.URL + "," + Blueprint.IMAGE_URL + "," + Blueprint.CATEGORY + " FROM " + Blueprint.FEED_TABLE;


        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE " + Blueprint.FEED_TABLE + " (" + Blueprint.TITLE + " TEXT,"
                        + Blueprint.DESCRIPTION + " TEXT," + Blueprint.TIME + " TEXT," +
                        Blueprint.URL + " TEXT," + Blueprint.IMAGE_URL + " TEXT," + Blueprint.CATEGORY + " TEXT);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS " + Blueprint.FEED_TABLE);
            this.onCreate(db);
        }



      public ArrayList<FeedItem> getSavedFeeds(){

            SQLiteDatabase writeableDatabase = this.getWritableDatabase();
            Cursor feedCursor = writeableDatabase.rawQuery(SELECT_ALL_FEEDS,null);
            ArrayList<FeedItem> savedFeeds = new ArrayList<>();

            if(feedCursor.moveToFirst()){

                do{

                    FeedItem feed = new FeedItem();

                    feed.setFeedTitle(feedCursor.getString(0));
                    feed.setFeedDescription(feedCursor.getString(1));
                    feed.setFeedTime(feedCursor.getString(2));
                    feed.setFeedURL(feedCursor.getString(3));
                    feed.setEnclosure(new FeedItem.Enclosure());
                    feed.setImgURL(feedCursor.getString(4));
                    feed.setFeedCategory(feedCursor.getString(5));

                    savedFeeds.add(feed);

                }while(feedCursor.moveToNext());
            }

            feedCursor.close();
            writeableDatabase.close();
            return savedFeeds;
        }

        public void addFeed(FeedItem feed){

            ContentValues contentValues = new ContentValues();
            contentValues.put(Blueprint.TITLE, feed.getFeedTitle());
            contentValues.put(Blueprint.DESCRIPTION, feed.getFeedDescription());
            contentValues.put(Blueprint.TIME, feed.getFeedTime());
            contentValues.put(Blueprint.URL, feed.getFeedURL());
            contentValues.put(Blueprint.IMAGE_URL, feed.getFeedImageURL());
            contentValues.put(Blueprint.CATEGORY, feed.getFeedCategory());

            SQLiteDatabase writeableDatabase = this.getWritableDatabase();
            writeableDatabase.insert(Blueprint.FEED_TABLE, Blueprint.TITLE, contentValues);
            writeableDatabase.close();
    }

        public void deleteFeed(String title)
        {
            SQLiteDatabase writeableDatabase = this.getWritableDatabase();
            writeableDatabase.delete(Blueprint.FEED_TABLE, Blueprint.TITLE + " = ? ", new String[] {title});
        }

        private static class Blueprint{

            private static final int VERSION = 1;
            private static final String DATABASE_NAME = "rssList.db";
            private static final String FEED_TABLE = "FEEDS";
            private static final String TITLE = "title";
            private static final String DESCRIPTION = "description";
            private static final String TIME = "time";
            private static final String URL = "url";
            private static final String IMAGE_URL = "img_url";
            private static final String CATEGORY = "category";

        }
    }
