package com.marijannovak.rssfeed;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

import static android.view.View.GONE;

public class FeedActivity extends Activity{

    private static final String BASE_URL = "http://www.bug.hr";
    private int lvState = 0;

    private ListView lvRSS;
    private ListView lvSaved;
    private FeedAdapter adapterRSS;
    private FeedAdapter adapterSaved;
    private ArrayList<FeedItem> rssList;
    private ArrayList<FeedItem> savedList;
    private ArrayList<FeedItem> filteredRSS;
    private ArrayList<FeedItem> filteredSaved;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Spinner categorySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_feed);

        init();
        setUpListViews();

    }

    private void init() {

        lvRSS = (ListView) findViewById(R.id.lvFeeds);
        lvRSS.setDivider(new ColorDrawable(Color.RED));
        lvRSS.setDividerHeight(2);

        lvSaved = (ListView) findViewById(R.id.lvSaved);
        lvSaved.setDivider(new ColorDrawable(Color.RED));
        lvSaved.setDividerHeight(2);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

               if(adapterRSS != null) updateRSS();
               else setUpListViews();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id)
        {
            case R.id.menuChange:

                changeView();

                return true;
        }

        return false;
    }


    private void changeView() {
        if(lvState == 0) {

            adapterSaved.updateFeeds(DatabaseHelper.getInstance(this).getSavedFeeds());

            lvRSS.setVisibility(GONE);
            lvSaved.setVisibility(View.VISIBLE);

            this.setTitle("Saved Feed");
            categorySpinner.setSelection(0);

            if(DatabaseHelper.getInstance(this).getSavedFeeds().size() == 0) Toast.makeText(FeedActivity.this, "No saved feeds! Add some by long pressing feeds from RSS", Toast.LENGTH_SHORT).show();

            lvState = 1;
        }

        else if(lvState == 1)
        {
            lvRSS.setVisibility(View.VISIBLE);
            lvSaved.setVisibility(GONE);
            this.setTitle("RSS Feed");
            categorySpinner.setSelection(0);

            lvState = 0;
        }

    }

    private void setUpListViews()
    {
        swipeRefreshLayout.setRefreshing(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        RSSApi api = retrofit.create(RSSApi.class);
        Call<AllFeeds> call = api.getFeeds();
        call.enqueue(new Callback<AllFeeds>() {
            @Override
            public void onResponse(Call<AllFeeds> call, Response<AllFeeds> response) {

                rssList = response.body().getFeedItems();

                makeCategorySpinner();

                fillRssLv();
                fillSavedLv();

                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<AllFeeds> call, Throwable t) {
                Log.d("Ode", t.getMessage());
                Toast.makeText(FeedActivity.this, "Error fetching data!", Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);

            }
        });

    }

    private void fillRssLv() {

        filteredRSS = filterFeeds(rssList, categorySpinner.getSelectedItem().toString());

        adapterRSS = new FeedAdapter(filteredRSS);
        lvRSS.setAdapter(adapterRSS);

        lvRSS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToWeb(position, rssList);
            }
        });

        lvRSS.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                FeedItem newFeed = filteredRSS.get(position);

                boolean doAdd = true;

                for(FeedItem feed : DatabaseHelper.getInstance(FeedActivity.this).getSavedFeeds())
                {
                    if(feed.getFeedURL().equals(newFeed.getFeedURL())) doAdd = false;
                }


                if(doAdd)
                {
                    DatabaseHelper.getInstance(FeedActivity.this).addFeed(newFeed);

                    //adapterSaved.addFeed(newFeed);
                    Toast.makeText(FeedActivity.this, "Item saved!", Toast.LENGTH_SHORT).show();
                }

                else
                {
                    Toast.makeText(FeedActivity.this, "Item already saved!", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    private void makeCategorySpinner() {
        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item , getCategories());
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categorySpinner.setAdapter(spinnerAdapter);
        categorySpinner.setSelection(0);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                filteredRSS = filterFeeds(rssList, categorySpinner.getItemAtPosition(position).toString());
                adapterRSS.updateFeeds(filteredRSS);

                filteredSaved = filterFeeds(DatabaseHelper.getInstance(FeedActivity.this).getSavedFeeds(), categorySpinner.getItemAtPosition(position).toString());
                adapterSaved.updateFeeds(filteredSaved);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void updateRSS()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        RSSApi api = retrofit.create(RSSApi.class);
        Call<AllFeeds> call = api.getFeeds();
        call.enqueue(new Callback<AllFeeds>() {
            @Override
            public void onResponse(Call<AllFeeds> call, Response<AllFeeds> response) {
                rssList = response.body().getFeedItems();
                adapterRSS.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(FeedActivity.this, "RSS Feed refreshed!", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<AllFeeds> call, Throwable t) {
                Log.d("Ode", t.getMessage());
                Toast.makeText(FeedActivity.this, "Error fetching data!", Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });


    }

    private ArrayList<FeedItem> filterFeeds(ArrayList<FeedItem> feedArray, String category)
    {
        ArrayList<FeedItem> filteredArray = new ArrayList<FeedItem>();

        if(category.equals("All categories")) filteredArray = feedArray;

        else {
            for (FeedItem feed : feedArray) {
                if (feed.getFeedCategory().equals(category)) filteredArray.add(feed);
            }
        }


        return filteredArray;
    }

    private ArrayList<String> getCategories()
    {
        ArrayList<String> categories = new ArrayList<>();
        categories.add("All categories");

        if(rssList != null) {
            for (FeedItem feed : rssList) {
                if (!categories.contains(feed.getFeedCategory()))
                    categories.add(feed.getFeedCategory());
            }
        }

        return categories;
    }

    private void goToWeb(int position, ArrayList<FeedItem> feedList)
    {
        Intent webIntent = new Intent();
        webIntent.setAction(Intent.ACTION_VIEW);
        String url = feedList.get(position).getFeedURL();
        Uri uri = Uri.parse(url);
        webIntent.setData(uri);

        startActivity(webIntent);

    }

    private void fillSavedLv() {


        savedList = DatabaseHelper.getInstance(this).getSavedFeeds();

        filteredSaved = filterFeeds(savedList, categorySpinner.getSelectedItem().toString());
        adapterSaved = new FeedAdapter(filteredSaved);
        lvSaved.setAdapter(adapterSaved);
        lvSaved.setVisibility(GONE);

        lvSaved.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToWeb(position, savedList);
            }
        });

        lvSaved.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                filteredSaved = filterFeeds(DatabaseHelper.getInstance(FeedActivity.this).getSavedFeeds(), categorySpinner.getSelectedItem().toString());
                adapterSaved.updateFeeds(filteredSaved);

                DatabaseHelper.getInstance(FeedActivity.this).deleteFeed(filteredSaved.get(position).getFeedTitle());
                adapterSaved.removeAt(position);

                Toast.makeText(FeedActivity.this, "Item removed!", Toast.LENGTH_SHORT).show();

                return true;
            }
        });

    }

}
