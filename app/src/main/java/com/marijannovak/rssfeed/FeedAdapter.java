package com.marijannovak.rssfeed;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marij on 9.4.2017..
 */

class FeedAdapter extends BaseAdapter {

    private List<FeedItem> feedList;

    public FeedAdapter(List<FeedItem> list)
    {
        this.feedList = list;
    }


    @Override
    public int getCount() {
        return feedList.size();
    }

    @Override
    public Object getItem(int position) {
        return feedList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_feed, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        FeedItem feedItem = this.feedList.get(position);

        viewHolder.tvTitle.setText(feedItem.getFeedTitle());
        viewHolder.tvContent.setText(feedItem.getFeedDescription());
        viewHolder.tvTime.setText(feedItem.getFeedTime());
        viewHolder.tvCategory.setText(feedItem.getFeedCategory());


        Picasso.with(parent.getContext())
                .load(feedItem.getFeedImageURL())
                .fit()
                .centerInside()
                .into(viewHolder.ivIcon);

        return convertView;
    }

    public void addFeed(FeedItem feed){
        this.feedList.add(feed);
        this.notifyDataSetChanged();
    }

    public void removeAt(int position)
    {
        this.feedList.remove(position);
        this.notifyDataSetChanged();
    }

    public void updateFeeds(ArrayList<FeedItem> feedItems)
    {
        this.feedList = feedItems;
        this.notifyDataSetChanged();
    }


    private static class ViewHolder{

        TextView tvTitle, tvContent, tvTime, tvCategory;
        ImageView ivIcon;

        public ViewHolder(View feedView)
        {
            tvTitle = (TextView) feedView.findViewById(R.id.tvTitle);
            tvContent = (TextView) feedView.findViewById(R.id.tvContent);
            tvTime = (TextView) feedView.findViewById(R.id.tvTime);
            tvCategory = (TextView) feedView.findViewById(R.id.tvCategory);
            ivIcon = (ImageView) feedView.findViewById(R.id.ivIcon);
        }

    }
}
