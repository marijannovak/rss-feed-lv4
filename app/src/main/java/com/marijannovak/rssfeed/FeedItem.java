package com.marijannovak.rssfeed;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by marij on 9.4.2017..
 */


@Root(name = "item", strict = false)
class FeedItem {

    @Element(name = "title" , required = false) private String feedTitle;
    @Element(name = "link" , required = false) private String feedURL;
    @Element(name = "pubDate" , required = false) private String feedTime;
    @Element(name = "description" , required = false) private String feedDescription;
    @Element(name = "category" , required = false)  private String feedCategory;

    @Element(name = "enclosure" , required = true) private Enclosure enclosure;

    @Root(name = "enclosure", strict = false)
    static class Enclosure{
        @Attribute(name = "type") private String enclosureType;
        @Attribute(name = "url") private String feedImageURL;
    }



    public String getFeedTitle() {
        return feedTitle;
    }

    public String getFeedURL() {
        return feedURL;
    }

    public String getFeedImageURL() {
        return enclosure.feedImageURL;
    }

    public String getFeedDescription() {
        return feedDescription;
    }

    public String getFeedCategory() {
        return feedCategory;
    }

    public String getFeedTime() {
        return feedTime;
    }

    public void setFeedTitle(String feedTitle) {
        this.feedTitle = feedTitle;
    }

    public void setFeedURL(String feedURL) {
        this.feedURL = feedURL;
    }

    public void setFeedTime(String feedTime) {
        this.feedTime = feedTime;
    }

    public void setFeedDescription(String feedDescription) {
        this.feedDescription = feedDescription;
    }

    public void setFeedCategory(String feedCategory) {
        this.feedCategory = feedCategory;
    }

    public void setImgURL(String URL)
    {
        this.enclosure.feedImageURL = URL;
    }

    public void setEnclosure(Enclosure enclosure)
    {
        this.enclosure = enclosure;
    }


}
