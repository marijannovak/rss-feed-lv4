package com.marijannovak.rssfeed;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by marij on 10.4.2017..
 */

interface RSSApi {

    @GET("/rss/vijesti/")
    Call<AllFeeds> getFeeds();

}
